
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/app.css">
<title>Inscription</title>
</head>
<body>
   <jsp:include page="navbar.jsp"></jsp:include>

	<div class="container-lg bg-dark mt-5">
	<h1 class="text-center">Inscription</h1>
		<form action="Servlet?flag=inscription" method="post">
			<div class="row">
				<div class="col-6">
					<div class="form-group">
						<label for="nom">Nom</label> <input type="text"
							class="form-control" name="nom" id="nom"
							placeholder="Enter votre nom">
					</div>
				</div>
				<div class="col-6">
					<div class="form-group">
						<label for="prenom">Prenom</label> <input type="text"
							class="form-control" id="prenom" name="prenom"
							placeholder="Entrez votre prenom">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-6">
					<div class="form-group">
						<label for="numRue">Numero de la rue</label> <input type="number"
							class="form-control" id="numRue" name="numRue"
							placeholder="Entrez le numero de la rue">
					</div>
				</div>
				<div class="col-6">
					<div class="form-group">
						<label for="prenom">Nom de la rue</label> <input type="text"
							class="form-control" id="nomRue" name="nomRue"
							placeholder="Entrez le nom de la rue">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-6">
					<div class="form-group">
						<label for="prenom">Code postal</label> <input type="text"
							class="form-control" id="cp" name="cp"
							placeholder="Entrez votre code postal">
					</div>
				</div>
				<div class="col-6">
					<div class="form-group">
						<label for="ville">Ville</label> <input type="text"
							class="form-control" id="ville" name="ville"
							placeholder="Entrez votre ville">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label for="tel">Telephone</label> <input type="text"
					class="form-control" id="tel" name="tel"
					placeholder="Entrez votre numero de telephone">
			</div>
			<div class="row">
				<div class="col-6">
					<button type="submit"
						class="btn btn-primary btn btn-large mt-3 form-control">S'inscrire</button>
				</div>
				<div class="col-6">
					<button type="reset"
						class="btn btn-danger btn btn-large mt-3 form-control">Effacer</button>
				</div>
			</div>
		</form>
	</div>

</body>
</html>
</body>
</html>