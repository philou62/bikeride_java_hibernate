<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/app.css">
<title>RideBike/Acceuil</title>
</head>
<body>
	<jsp:include page="navbar.jsp"></jsp:include>
	
	<h1 class="text-center mt-5">Bienvenue sur BikeRide</h1>
	
	
	
	
	
	<script type="text/javascript" src="js/app.js"></script>
	<script type="text/javascript" src="js/bootstrap.bundle.min.js"></script>
</body>
</html>