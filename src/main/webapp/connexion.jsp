<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/app.css">
<title>bikeRide/Connexion</title>
</head>
<body>
	<jsp:include page="navbar.jsp"></jsp:include>

	<div class="container-lg bg-dark mt-5" id="containerInscription">
		<h1 class="text-center">Connexion</h1>
		<form action="Servlet?flag=connexion" method="post">

			<div class="row">
				<div class="col-6">
					<div class="form-group">
						<label for="login" class="form-label mt-4">Pseudo</label> <input
							type="text" id="login" name="login" class="form-control"
							placeholder="Renseigner votre login">
					</div>
				</div>
				<div class="col-6">
					<div class="form-group">
						<label for="pwd" class="form-label mt-4">Mot de passe</label> <input
							type="password" id="pwd" name="pwd" class="form-control"
							placeholder="Renseigner votre mot de passe">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-6">
					<button type="submit"
						class="btn btn-primary btn btn-large mt-3 form-control">Se connecter</button>
				</div>
				<div class="col-6">
					<button type="reset"
						class="btn btn-danger btn btn-large mt-3 form-control">Effacer</button>
				</div>
			</div>
		</form>
	</div>



	<script type="text/javascript" src="js/app.js"></script>
	<script type="text/javascript" src="js/bootstrap.bundle.min.js"></script>
</body>
</html>