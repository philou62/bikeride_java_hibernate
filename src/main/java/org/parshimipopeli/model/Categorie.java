package org.parshimipopeli.model;

import java.util.List;

import javax.persistence.*;

@Entity
public class Categorie {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_categorie;
	private String nom_cat;
	private String deignation_cat;
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name = "ID_CATEGORIE")
	private List<Article> articles;
	
	public Categorie(String nom_cat, String deignation_cat, List<Article> articles) {
		this.nom_cat = nom_cat;
		this.deignation_cat = deignation_cat;
		this.articles = articles;
	}

	public String getNom_cat() {
		return nom_cat;
	}

	public void setNom_cat(String nom_cat) {
		this.nom_cat = nom_cat;
	}

	public String getDeignation_cat() {
		return deignation_cat;
	}

	public void setDeignation_cat(String deignation_cat) {
		this.deignation_cat = deignation_cat;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

	public int getId_categorie() {
		return id_categorie;
	}

	@Override
	public String toString() {
		return "Categorie [id_categorie=" + id_categorie + ", nom_cat=" + nom_cat + ", deignation_cat=" + deignation_cat
				+ ", articles=" + articles + "]";
	}
	
	public void ListCategorie() {
		
	}
	
	public void ListArticle(List<Article> articles) {
		this.articles = articles;
	}
	
}
