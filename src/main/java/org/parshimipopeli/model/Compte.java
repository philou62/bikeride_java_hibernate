package org.parshimipopeli.model;

import javax.persistence.*;


@Entity
@Table(name = "compte")
public class Compte {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "compte_id")
	private int idCompte;
	private String login;
	private String pwd;
	private String type = "s";
	
	public Compte(String login, String pwd, String type) {
		this.login = login;
		this.pwd = pwd;
		this.type = type;
	}
	
	public Compte() {
		
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getIdCompte() {
		return idCompte;
	}

	@Override
	public String toString() {
		return "Compte [idCompte=" + idCompte + ", login=" + login + ", pwd=" + pwd + ", type=" + type + "]";
	}
	

	
	
	}

	
