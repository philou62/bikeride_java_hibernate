package org.parshimipopeli.model;

import java.util.List;

import javax.persistence.*;

import org.hibernate.validator.constraints.Length;

@Entity
@Table
//@Table(utilisateur) pour changer de nom de table a la place de Users
public class Users {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idUsers;
	@Length(min = 3, max = 30)
	private String nom;
	@Length(min = 3, max = 30)
	private String prenom;
	@Length(min = 3, max = 30)
	private String numRue;
	private String nomRue;
	private String cp;
	private String ville;
	private String tel;
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name = "ID_USER")
	private List<Compte> comptes;
	
	
	
	public Users(String nom, String prenom, String numRue, String nomRue, String cp, String ville, String tel) {
		this.nom = nom;
		this.prenom = prenom;
		this.numRue = numRue;
		this.nomRue = nomRue;
		this.cp = cp;
		this.ville = ville;
		this.tel = tel;
	}
	public Users() {
		// TODO Auto-generated constructor stub
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getNumRue() {
		return numRue;
	}
	public void setNumRue(String numRue) {
		this.numRue = numRue;
	}
	public String getNomRue() {
		return nomRue;
	}
	public void setNomRue(String nomRue) {
		this.nomRue = nomRue;
	}
	public String getCp() {
		return cp;
	}
	public void setCp(String cp) {
		this.cp = cp;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public int getIdUsers() {
		return idUsers;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	@Override
	public String toString() {
		return "Users [idUsers=" + idUsers + ", nom=" + nom + ", prenom=" + prenom + ", numRue=" + numRue + ", nomRue="
				+ nomRue + ", cp=" + cp + ", ville=" + ville + ", tel=" + tel + "]";
	}
	public void listUsers() {
		// TODO Auto-generated method stub
		
	}
	public void ListComptes(List<Compte> comptes) {
		this.comptes = comptes;
		
	}
	
	
	
	
	
}
