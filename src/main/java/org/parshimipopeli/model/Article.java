package org.parshimipopeli.model;

import javax.persistence.*;

@Entity
public class Article {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_article;
	private String designation;
	private double prixUnitaire;
	private int quantite;
	
	
	public Article(String designation, double prixUnitaire, int quantite) {
		super();
		this.designation = designation;
		this.prixUnitaire = prixUnitaire;
		this.quantite = quantite;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public double getPrixUnitaire() {
		return prixUnitaire;
	}

	public void setPrixUnitaire(double prixUnitaire) {
		this.prixUnitaire = prixUnitaire;
	}

	public int getQuantite() {
		return quantite;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

	public int getId_article() {
		return id_article;
	}

	@Override
	public String toString() {
		return "Articles [id_article=" + id_article + ", designation=" + designation + ", prixUnitaire=" + prixUnitaire
				+ ", quantite=" + quantite + "]";
	}
	

}
