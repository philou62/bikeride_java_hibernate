package org.parshimipopeli.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.*;
import org.hibernate.cfg.Configuration;
import org.parshimipopeli.model.Compte;
import org.parshimipopeli.model.Users;

public class HibernateDao {
	
	Configuration cfg = new Configuration().configure();
	SessionFactory sf = cfg.buildSessionFactory();
	Session session = sf.openSession();
	Transaction tr = session.beginTransaction();
	
	public void commitAndClose() {
		tr.commit();
		session.close();
		sf.close();
	}

	public void ajoutUsers(Users user) {
		tr.begin();
		session.persist(user);
		}

	public void deleteUsers(Users user) {
		
	}
	
	public void listUsers() {
		//tr.begin();
		Criteria cr = session.createCriteria(Users.class);
		List<Users> lUsers = (List<Users>) cr.list();
		for(Users usr : lUsers) {
			System.out.println(usr);
		}
	}
	
	public void ajoutCompte(Compte cpt) {
		tr.begin();
		session.persist(cpt);
		
	}

	public String verifCoordonnees(String login) {
		String mdp = null;
		mdp = "select pwd from compte where login like '" + login + "'";
		return null;
	}
}
