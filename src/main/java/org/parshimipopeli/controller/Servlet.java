package org.parshimipopeli.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.parshimipopeli.dao.HibernateDao;
import org.parshimipopeli.model.Compte;
import org.parshimipopeli.model.Users;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class theServlet
 */
@WebServlet("/Servlet")
public class Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	public Servlet() {
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String flag = request.getParameter("flag");
		if (flag.equalsIgnoreCase("inscription")) {
			this.doInscription(request, response);
		} else if (flag.equalsIgnoreCase("connexion")) {
			this.doConnexion(request, response);
		} else {
			this.doGet(request, response);
		}
	}

	private void doInscription(HttpServletRequest request, HttpServletResponse response) {
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String noRue = request.getParameter("numRue");
		String nomRue = request.getParameter("nomRue");
		String cp = request.getParameter("cp");
		String ville = request.getParameter("ville");
		String tel = request.getParameter("tel");
		String login = request.getParameter("login");
		String pwd = request.getParameter("pwd");
		Users u = new Users(nom, prenom, noRue, nomRue, cp, ville, tel);
		HibernateDao hd = new HibernateDao();
		hd.ajoutUsers(u);
		Compte cpt = new Compte(login, pwd, "S");
		hd.ajoutCompte(cpt);

		List<Compte> lCompte = new ArrayList<Compte>();
		lCompte.add(cpt);
		u.ListComptes(lCompte);
		hd.commitAndClose();
	};

	private void doConnexion(HttpServletRequest request, HttpServletResponse response) {
		String login = request.getParameter("login");
		String pass = request.getParameter("pwd");
		HibernateDao hd = new HibernateDao();
		String passBDD = hd.verifCoordonnees(login);
		if (pass == null) {
			request.getRequestDispatcher("/connexion.jsp");
		} else if (pass.equals(passBDD)) {
			request.getRequestDispatcher("/connOk");
		}
	}
}
